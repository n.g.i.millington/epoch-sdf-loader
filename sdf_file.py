import collections
import logging

from .error_messager import ErrorMessager
from .shared_methods import *

logging.basicConfig()

class SdfFile:
    """
    This class handles all file handling operations for the routine, providing getter and setter
    routines  for all variables involved in accessing a file.  It also provides  wrapper routines
    and functions for finding a location within a file and retrieving data from that file.

    IMPORTS:
    numpy as np
    CaseFactory as sfact

    PARAMETERS:
    file_name : string

    """

    def __init__(self, file_name, **kwargs):
        self.logger = logging.getLogger(__name__)

        verbose = kwargs.get("verbose", False)
        if verbose:
            self.logger.setLevel(logging.debug)
        self.input_dir = kwargs.get("input_dir", "")
        self.logger.info("input directory set to " + self.input_dir)

        self.name = file_name
        self.file_path = self.input_dir + file_name
        self.f_handle = open(self.file_path, "rb")

        # Header dictionary replacement.  Allows for much easier usage
        self.header = None
        self.endliness = None
        self.version = None
        self.revision = None
        self.code_name = None
        self.first_block = None
        self.summary_loc = None
        self.summary_size = None
        self.num_blocks = None
        self.header_length = None
        self.step = None
        self.time = None
        self.jobid1 = None
        self.jobid2 = None
        self.str_length = None
        self.code_io = None

        self.block_register_d = collections.OrderedDict()

        self.build_header()
        self.build_block_register()

    def close_file(self):
        """Closes the file .  Only done once the Sdffile is deconstructed."""
        self.f_handle.close()

    def build_header(self):
        """Parses the file contained within f_handle and builds the file header for later reference."""
        self.logger.info("building file header")
        self.header = conv_uchar_str(np.fromfile(self.f_handle, dtype=np.uint8, count=4))
        self.endliness = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.version = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.revision = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.code_name = conv_uchar_str(np.fromfile(self.f_handle, dtype=np.uint8, count=32))
        self.first_block = np.fromfile(self.f_handle, dtype=np.int64, count=1)[0]
        self.summary_loc = np.fromfile(self.f_handle, dtype=np.int64, count=1)[0]
        self.summary_size = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.num_blocks = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.header_length = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.step = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.time = np.fromfile(self.f_handle, dtype=np.float64, count=1)[0]
        self.jobid1 = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.jobid2 = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.str_length = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.code_io = np.fromfile(self.f_handle, dtype=np.int32, count=1)[0]
        self.logger.info("file header built")

    def build_block_register(self):
        """To allow for efficient finding of blocks and to prevent having to build a whole legion of data
        output blocks, the SdfFile class builds a block register which names each block and gives its
        location in the file.  This reduces any attempt to find the block to a one line hashed comparison."""

        # Run through the list of blocks to the maximum number of blocks
        # It saves first the location of the next block, then the name

        self.logger.info("building block register")
        next_block_loc = self.first_block
        for block_reg_count in range(0, self.num_blocks):
            # Find the initial position in the block
            self.f_handle.seek(next_block_loc)
            cur_block_loc = next_block_loc
            next_block_loc = np.fromfile(self.f_handle, dtype=np.int64, count=1)[0]
            # This is set to seek from current location with an offset of 8 bytes
            self.f_handle.seek(8, 1)
            block_name = conv_uchar_str(np.fromfile(self.f_handle, dtype=np.uint8, count=32))
            self.block_register_d[block_name] = cur_block_loc
            self.f_handle.seek(next_block_loc)
            self.logger.info("block name " + block_name + " registered")

        self.logger.info("block register is built")
        if self.logger.lvl == "info":
            self.logger.info("printing block register")
            print(self.block_register_d)

    def print_blocks_in_sdf_file(self):
        print("Blocks in current sdf_file:  ", self.name)
        for key, val in self.block_register_d.items():
            print(key)

# Unit tests
