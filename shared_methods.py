from itertools import combinations

import numpy as np

def select_block_data(s_block_o):
    """
    Interface routine which allows the data block in question to decide which data block it is
    building using the data type in question.  I may just pass the variable in question to the case
    factory since there is no need for there to be an interface here.
    """
    pass

def conv_uchar_str(uchar_array_l):
    """
    Takes a series of integer values and converts them first letter by letter into strings,
    then concatenates them together as a string.  Used in a number of routines for a variety of
    different classes so I consider this a common interface.
    """
    out_s = ''
    for uchar in uchar_array_l:
        out_s += chr(uchar)

    out_s = out_s.replace(' ', '')
    out_s = out_s.replace('\x00', '')
    return out_s


def meta_point_var(s_block, s_file):
    """Returns the meta data using the point variable."""
    s_block.logger.info("returning meta data for point variable")
    meta_variable(s_block, s_file)
    s_block.num_particles = np.fromfile(s_file.f_handle, dtype=np.int64, count=1)[0]
    s_block.logger.info("meta data complete")

    s_block.vals = dict(normalisation=s_block.normalisation,
                        axis_units=s_block.axis_units,
                        mesh_id=s_block.mesh_id,
                        num_particles=s_block.num_particles)


def meta_plain_var(s_block, s_file):
    """
    Returns the metadata for a plain variable
    """
    s_block.logger.info("returning meta data for plain variable")
    meta_variable(s_block, s_file)
    s_block.grid_points = []

    # Sets up grid points for each axis limited by the number of dimensions
    check_sum = 1.0
    for dim_num_i in range(0, s_block.num_dims):
        s_block.grid_points.append(np.fromfile(s_file.f_handle, dtype=np.int32, count=1)[0])
        s_block.logger.info("returning value of grid points for " + str(dim_num_i) + " : " + str(s_block.grid_points))
        check_sum *= s_block.grid_points[dim_num_i]

    if check_sum < s_block.data_length or check_sum > s_block.data_length:
        s_block.logger.warning("checksum incorrect - possible file corruption, assuming 2D default")
        s_block.grid_points[1] = int(s_block.data_length/s_block.grid_points[0])

    s_block.logger.info("returning value of grid points" + str(s_block.grid_points))
    s_block.grid_points = tuple(s_block.grid_points)
    s_block.stagger = np.fromfile(s_file.f_handle, dtype=np.int32, count=1)[0]
    s_block.logger.info("meta data complete")

    s_block.vals = dict(normalisation=s_block.normalisation,
                        axis_units=s_block.axis_units,
                        mesh_id=s_block.mesh_id,
                        grid_points=s_block.grid_points,
                        stagger=s_block.stagger)


def meta_plain_mesh(s_block, s_file):
    """
    Returns the meta data for a plain mesh
    Note this that is used in order to reference other meshes
    """
    s_block.logger.info("returning meta data for plain mesh")
    meta_mesh(s_block, s_file)
    s_block.grid_points = []
    check_sum = 1.0
    # Sets up normalisation values for each axis limited by the number of dimensions
    for dim_num_i in range(0, s_block.num_dims):
        s_block.grid_points.append(np.fromfile(s_file.f_handle, dtype=np.int32, count=1)[0])
        check_sum *= s_block.grid_points[dim_num_i]

    if check_sum < s_block.data_length or check_sum > s_block.data_length:
        s_block.logger.warning("checksum incorrect - possible file corruption, assuming 2D default")
        s_block.grid_points[1] = int(s_block.data_length/s_block.grid_points[0])

    s_block.grid_points = tuple(s_block.grid_points)
    s_block.logger.info("meta data complete")

    s_block.vals = dict(normalisation=s_block.normalisation,
                        axis_units=s_block.axis_units,
                        axis_labels=s_block.axis_labels,
                        geometry=s_block.geometry,
                        min_value=s_block.min_value,
                        max_value=s_block.max_value,
                        grid_points=s_block.grid_points)


def meta_point_mesh(s_block, s_file):
    s_block.logger.info("returning meta data for point mesh")
    meta_mesh(s_block, s_file)
    s_block.num_particles = []
    # Sets up normalisation values for each axis limited by the number of dimensions
    for dim_num_i in range(0, s_block.num_dims):
        s_block.num_particles.append(np.fromfile(s_file.f_handle, dtype=np.int32, count=1)[0])
    s_block.num_particles = tuple(s_block.num_particles)
    s_block.logger.info("meta data complete")

    s_block.vals = dict(normalisation=s_block.normalisation,
                        axis_units=s_block.axis_units,
                        axis_labels=s_block.axis_labels,
                        geometry=s_block.geometry,
                        min_value=s_block.min_value,
                        max_value=s_block.max_value,
                        num_particles=s_block.num_particles)


def meta_ignore(s_block, s_file):
    s_block.logger.info("no meta data is implemented/required")
    s_block.logger.info("meta data complete")


def meta_mesh(s_block, s_file):
    """
    Common case factory constructor used for all reuseable code within meta data construction.
    Worth it to avoid code reuse and in case headers are changed in EPOCH runs
    """

    s_block.normalisation = []
    s_block.axis_labels = []
    s_block.axis_units = []
    s_block.min_value = []
    s_block.max_value = []

    # Sets up normalisation values for each axis limited by the number of dimensions
    for dim_num_i in range(0, s_block.num_dims):
        s_block.normalisation.append(np.fromfile(s_file.f_handle, dtype=np.float64, count=1)[0])
    s_block.normalisation = tuple(s_block.normalisation)

    # Sets up labels for each axis limited by the number of dimensions
    for dim_num_i in range(0, s_block.num_dims):
        s_block.axis_labels.append(conv_uchar_str(np.fromfile(s_file.f_handle, dtype=np.uint8, count=32)))
    s_block.axis_labels = tuple(s_block.axis_labels)

    # Sets up labels for each axis limited by the number of dimensions
    for dim_num_i in range(0, s_block.num_dims):
        s_block.axis_units.append(conv_uchar_str(np.fromfile(s_file.f_handle, dtype=np.uint8, count=32)))
    s_block.axis_units = tuple(s_block.axis_units)
    s_block.geometry = np.fromfile(s_file.f_handle, dtype=np.int32, count=1)[0]

    # Sets up minimum normalisation values for each axis limited by the number of dimensions
    for dim_num_i in range(0, s_block.num_dims):
        s_block.min_value.append(np.fromfile(s_file.f_handle, dtype=np.float64, count=1)[0])
    s_block.min_value = tuple(s_block.min_value)

    # Sets up maximum normalisation values for each axis limited by the number of dimensions
    for dim_num_i in range(0, s_block.num_dims):
        s_block.max_value.append(np.fromfile(s_file.f_handle, dtype=np.float64, count=1)[0])
    s_block.max_value = tuple(s_block.max_value)


def meta_variable(s_block, s_file):
    """
    Common case factory constructor used for all reuseable code within meta data construction.
    Worth it to avoid code reuse and in case headers are changed in EPOCH runs
    """
    s_block.normalisation = np.fromfile(s_file.f_handle, dtype=np.float64, count=1)[0]
    s_block.axis_units = conv_uchar_str(np.fromfile(s_file.f_handle, dtype=np.uint8, count=32))
    s_block.mesh_id = conv_uchar_str(np.fromfile(s_file.f_handle, dtype=np.uint8, count=32))


# Data sections

def data_ignore(s_block, s_file):
    print("Not yet implemented or a block that should be ignored")

# It is probably possible to abstract this by data type but it may not be necessary
# This all needs to be properly named

def data_point_var(s_block, s_file):
    s_block.logger.info("returning data for point variable")
    s_file.f_handle.seek(s_block.data_location)
    s_block.data = np.fromfile(s_file.f_handle, dtype=s_block.data_type, count=int(s_block.data_length))


def data_plain_var(s_block, s_file):
    s_block.logger.info("returning data for plain variable")
    s_file.f_handle.seek(s_block.data_location)
    s_block.data = np.fromfile(s_file.f_handle,
                               dtype=s_block.data_type,
                               count=int(s_block.data_length))
    s_block.data = np.reshape(s_block.data, s_block.grid_points, order='F')
    s_block.logger.info("mesh data complete")


def data_plain_mesh(s_block, s_file):
    s_block.logger.info("returning meta data for plain mesh")
    s_file.f_handle.seek(s_block.data_location)
    s_block.data = np.fromfile(s_file.f_handle,
                               dtype=s_block.data_type,
                               count=int(s_block.data_length))

    s_block.data = np.reshape(s_block.data,
                              s_block.grid_points,
                              order='F')
    s_block.logger.info("mesh data complete")


def data_point_mesh(s_block, s_file):
    s_block.logger.info("returning data for point mesh")
    s_file.f_handle.seek(s_block.data_location)
    s_block.data = np.fromfile(s_file.f_handle, dtype=s_block.data_type,
                               count=int(s_block.data_length*s_block.num_dims))
    #s_block.data = np.reshape(s_block.data, (s_block.num_particles, s_block.num_dims))
    # Add this in for multidimensional data later once headache is clear NMQUB
